$(document).ready(function() {
    // queryLoader2
    $('body').queryLoader2({
        barColor: 'transparent',
        backgroundColor: 'transparent',
        percentage: false,
        barHeight: 1,
        fadeOutTime: 1000,
        onProgress: function (percentage) {
            if (percentage > 90) {
                $('.b-mask').fadeOut()
            }
        }
    });
});